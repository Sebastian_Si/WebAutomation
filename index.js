const puppeteer = require('puppeteer');
const fs = require('fs');

// config
const openBrowser = false;
const log = false;
const url = 'https://porofessor.gg/';
const imagePath = 'images/';
const intervall = 5000;

// setup
if (!fs.existsSync(imagePath)){
  fs.mkdirSync(imagePath);
}

let counter = 1;

const run = async () => {
    const browser = await puppeteer.launch({headless: !openBrowser});
    const page = await browser.newPage();
    if(log) page.on('console', (msg) => console.log('>>', msg.text()));
    await page.goto(url, {waitUntil: 'networkidle2'});
    await page.evaluate(() => {
      document.querySelector('body > main > div:nth-child(2) > form > div.inputLineContainer > div > input[type=text]').value = 'Amicos Cusdos';
      document.querySelector('body > main > div:nth-child(2) > form > div.inputLineContainer > div > button').click();
    });
    await page.waitForNavigation({ waitUntil: 'networkidle0' });
    console.log(`[${new Date().toLocaleString()}] taking screenshot number ${counter}`);
    await page.screenshot({ path: `${imagePath}${counter++}.png`});
    await browser.close();
  };

setInterval(async () => await run(), intervall);