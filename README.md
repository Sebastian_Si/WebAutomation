# Web-Automation Template
This is a Template to do some basic automation stuff on web pages.

## Installation
Too run this project node is required. You can get it from [here](https://nodejs.org/en/).

To intall the requirements run ```npm install```

## Run
To run the project use ```npm start```
